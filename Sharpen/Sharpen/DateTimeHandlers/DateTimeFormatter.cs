﻿using System;

namespace Sharpen.DateTimeHandlers
{
	public class DateTimeFormatter
	{
		public String Pattern { get; private set;}

		public DateTimeFormatter ()
		{
		}

		public static DateTimeFormatter OfPattern(String pattern) 
		{
			DateTimeFormatter dtf = new DateTimeFormatter();
			dtf.Pattern = pattern;

			return dtf;
		}

	}
}

