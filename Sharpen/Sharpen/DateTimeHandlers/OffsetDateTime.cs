﻿using System;
using System.Runtime.Serialization;

namespace Sharpen.DateTimeHandlers
{
	[DataContract]
	public class OffsetDateTime : Temporal
	{
		public static readonly OffsetDateTime Max = new OffsetDateTime(DateTimeOffset.MaxValue);
		public static readonly OffsetDateTime Min = new OffsetDateTime(DateTimeOffset.MinValue);

		[DataMember]
		private DateTimeOffset _dateTime;

		public OffsetDateTime ()
		{
		}

		private OffsetDateTime(DateTimeOffset dateTime)
		{
			this._dateTime = dateTime;
		}

		public int GetDayOfMonth() 
		{
			return this._dateTime.Day;
		}

		public int GetMonthValue()
		{
			return this._dateTime.Month;
		}

		public long Ticks 
		{
			get {
				return this._dateTime.Ticks;
			}
		}

		public int GetYear()
		{
			return this._dateTime.Year;
		}

		public static OffsetDateTime Now()
		{
			return new OffsetDateTime (DateTimeOffset.Now);
		}

		public static OffsetDateTime Now(ZoneId id)
		{
			DateTimeOffset dto = DateTimeOffset.Now.ToOffset (id.Offset);
			return new OffsetDateTime(dto);
		}

		public static OffsetDateTime Of(int year, int month, int day, int hour, int minute, int second, long nanoOfSecond, ZoneOffset offset)
		{
			const long nanosecondsPerTick = 100;
			DateTimeOffset dto = new DateTimeOffset (year, month, day, hour, minute, second, offset.Offset);
			dto = dto.AddTicks(nanoOfSecond / nanosecondsPerTick);
			return new OffsetDateTime(dto);
		}

		public static OffsetDateTime Of(LocalDateTime localDateTime, ZoneOffset offset)
		{
			DateTimeOffset newdate = new DateTimeOffset (localDateTime.Ticks, offset.Offset);
			return new OffsetDateTime(newdate);
		}

		public int CompareTo(OffsetDateTime other)
		{
			return this._dateTime.CompareTo(other._dateTime);
		}

		public string Format(DateTimeFormatter formatter) 
		{
			return this._dateTime.ToString (formatter.Pattern);
		}
			
		public bool IsAfter(OffsetDateTime other)
		{
			return this._dateTime.CompareTo(other._dateTime) > 0;
		}

		public bool IsBefore(OffsetDateTime other)
		{
			return this._dateTime.CompareTo(other._dateTime) < 0;
		}

		public bool IsEqual(OffsetDateTime other)
		{
			return this._dateTime.CompareTo(other._dateTime) == 0;
		}

		public OffsetDateTime MinusSeconds(double seconds)
		{
			return new OffsetDateTime (this._dateTime.AddSeconds (-seconds));
		}

		public OffsetDateTime PlusDays(double days)
		{
			return new OffsetDateTime (this._dateTime.AddDays (days));
		}

		public OffsetDateTime PlusNanos(long nanoseconds)
		{
			const double nanosecondsPerSecond = 1e9;
			DateTimeOffset newDate = this._dateTime.AddSeconds (nanoseconds / nanosecondsPerSecond);
			return new OffsetDateTime(newDate);
		}

		public OffsetDateTime PlusSeconds(double seconds)
		{
			return new OffsetDateTime (this._dateTime.AddSeconds (seconds));
		}

		public static OffsetDateTime Parse(string stringToParse, DateTimeFormatter formatter)
		{
			DateTimeOffset newdate = DateTimeOffset.ParseExact (stringToParse, formatter.Pattern, System.Globalization.DateTimeFormatInfo.InvariantInfo);
			return new OffsetDateTime (newdate);
		}

		public ZonedDateTime AtZoneSameInstant(ZoneId newZone)
		{
			DateTimeOffset newDate = new DateTimeOffset (this._dateTime.Ticks, newZone.Offset);
			return new ZonedDateTime (newDate);
		}
	}
}

