﻿using System;

namespace Sharpen.DateTimeHandlers
{
	public class LocalDateTime : Temporal
	{
		private DateTime _dateTime;

		public LocalDateTime ()
		{
		}

		private LocalDateTime(DateTime dateTime)
		{
			this._dateTime = dateTime;
		}

		public int GetDayOfMonth() 
		{
			return this._dateTime.Day;
		}

		public int GetMonthValue()
		{
			return this._dateTime.Month;
		}

		public long Ticks 
		{
			get {
				return this._dateTime.Ticks;
			}
		}

		public int GetYear()
		{
			return this._dateTime.Year;
		}

		public static LocalDateTime Now()
		{
			return new LocalDateTime (DateTime.Now);
		}

		public static LocalDateTime Of(int year, int month, int day, int hour, int minute, int second, long nanoOfSecond)
		{
			const long nanosecondsPerTick = 100;
			DateTime dto = new DateTime (year, month, day, hour, minute, second);
			dto = dto.AddTicks(nanoOfSecond / nanosecondsPerTick);
			return new LocalDateTime(dto);
		}

		public int CompareTo(LocalDateTime other)
		{
			return this._dateTime.CompareTo(other._dateTime);
		}

		public string Format(DateTimeFormatter formatter) 
		{
			return this._dateTime.ToString (formatter.Pattern);
		}

		public bool IsAfter(LocalDateTime other)
		{
			return this._dateTime.CompareTo(other._dateTime) > 0;
		}

		public bool IsBefore(LocalDateTime other)
		{
			return this._dateTime.CompareTo(other._dateTime) < 0;
		}

		public bool IsEqual(LocalDateTime other)
		{
			return this._dateTime.CompareTo(other._dateTime) == 0;
		}

		public static LocalDateTime Parse(string stringToParse, DateTimeFormatter formatter)
		{
			DateTime newdate = DateTime.ParseExact (stringToParse, formatter.Pattern, System.Globalization.DateTimeFormatInfo.InvariantInfo);
			return new LocalDateTime (newdate);
		}
	}
}

