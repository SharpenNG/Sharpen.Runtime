﻿using System;

namespace Sharpen
{
	public interface Temporal
	{
		long Ticks { get;}
	}
}

