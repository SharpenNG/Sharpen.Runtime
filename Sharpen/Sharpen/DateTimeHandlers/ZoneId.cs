﻿using System;

namespace Sharpen.DateTimeHandlers
{
	public class ZoneId
	{
		private TimeZoneInfo _tzInfo;

		public TimeSpan Offset 
		{
			get {
				return _tzInfo.BaseUtcOffset;
			}
		}

		private ZoneId ()
		{
		}

		private ZoneId(TimeZoneInfo tzInfo)
		{
			this._tzInfo = tzInfo;
		}

		public static ZoneId Of(String zoneId)
		{
			if (String.IsNullOrEmpty(zoneId) || String.Equals("Z",zoneId,StringComparison.OrdinalIgnoreCase)){
				return new ZoneId (TimeZoneInfo.Utc);
			}

			foreach(TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones()) 
			{
				if (String.Equals (zoneId, z.DisplayName, StringComparison.OrdinalIgnoreCase)
				   || String.Equals (zoneId, z.StandardName, StringComparison.OrdinalIgnoreCase)
				   || String.Equals (zoneId, z.DaylightName, StringComparison.OrdinalIgnoreCase)) {
					return new ZoneId (z);
				}
			}

			throw new ArgumentException (zoneId + " is not recognized");
		}
	}
}

