﻿using System;

namespace Sharpen.DateTimeHandlers
{
	public class ZoneOffset
	{
		public static readonly ZoneOffset Utc = new ZoneOffset();

		public TimeSpan Offset { get; private set; }

		private ZoneOffset ()
		{
			this.Offset = new TimeSpan (0);
		}
	}
}

