﻿using System;

namespace Sharpen.DateTimeHandlers
{
	public class Duration
	{
		private const long NanosecondsPerMillisecond = 1000000;
		private const long NanosecondsPerTick = 100;

		private TimeSpan _span;

		private Duration ()
		{
			this._span = new TimeSpan (0);
		}

		private Duration(TimeSpan span)
		{
			this._span = span;
		}

		public static Duration Between(Temporal t1, Temporal t2)
		{
			return new Duration (new TimeSpan (t1.Ticks - t2.Ticks));
		}

		public long GetSeconds()
		{
			return this._span.Seconds;
		}

		public long ToMillis()
		{
			return this._span.Ticks * NanosecondsPerTick / NanosecondsPerMillisecond;
		}

		public long ToNanos()
		{
			return this._span.Ticks * NanosecondsPerTick;
		}
	}
}

