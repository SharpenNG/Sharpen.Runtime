﻿using System;
using Sharpen.DateTimeHandlers;

namespace Sharpen
{
	public class ZonedDateTime : Temporal
	{
		private DateTimeOffset _dateTime;

		public ZonedDateTime ()
		{
		}

		public long Ticks 
		{
			get {
				return this._dateTime.Ticks;
			}
		}

		public ZonedDateTime(DateTimeOffset dateTime)
		{
			this._dateTime = dateTime;
		}

		public string Format(DateTimeFormatter formatter) 
		{
			return this._dateTime.ToString (formatter.Pattern);
		}
	}
}

