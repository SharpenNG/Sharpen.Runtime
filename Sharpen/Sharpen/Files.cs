﻿using System;
using System.Collections.Generic;

namespace Sharpen
{
	public static class Files
	{
		public static List<string> ReadAllLines(FilePath path)
		{
			string[] lines = System.IO.File.ReadAllLines (path.ToString());
			return new List<string> (lines);
		}
	}
}

