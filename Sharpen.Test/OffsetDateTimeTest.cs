﻿using System;
using NUnit.Framework;
using Sharpen.DateTimeHandlers;

namespace Sharpen.Test
{
	[TestFixture]
	public class OffsetDateTimeTest
	{
		public OffsetDateTimeTest ()
		{
		}

		[Test]
		public void TestParser() 
		{
			string formatString = "yyyy MM dd HH:mm:ss.FFF K";
			string valueString = "2010 05 01 02:03:04.567 Z";
			int year = 2010;
			int month = 5;
			int day = 1;
			int hour = 2;
			int minute = 3;
			int second = 4;
			long nanoseconds = 567000000;

			OffsetDateTime expected = OffsetDateTime.Of (year, month, day, hour, minute, second, nanoseconds, ZoneOffset.Utc);
			OffsetDateTime actual = OffsetDateTime.Parse (valueString, DateTimeFormatter.OfPattern (formatString));
			Assert.IsTrue (expected.IsEqual(actual));
		}


		[Test]
		public void TestLocalDateInitializer() 
		{
			int year = 2010;
			int month = 5;
			int day = 1;
			int hour = 2;
			int minute = 3;
			int second = 4;
			long nanoseconds = 567000000;

			OffsetDateTime expected = OffsetDateTime.Of (year, month, day, hour, minute, second, nanoseconds, ZoneOffset.Utc);
			LocalDateTime lclDate = LocalDateTime.Of (year, month, day, hour, minute, second, nanoseconds);
			OffsetDateTime actual = OffsetDateTime.Of (lclDate, ZoneOffset.Utc);
			Assert.IsTrue (expected.IsEqual(actual));
		}

	}
}

