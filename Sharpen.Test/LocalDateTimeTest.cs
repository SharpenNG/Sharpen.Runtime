﻿using System;
using NUnit.Framework;
using Sharpen.DateTimeHandlers;

namespace Sharpen.Test
{
	[TestFixture]
	public class LocalDateTimeTest
	{
		public LocalDateTimeTest ()
		{
		}

		[Test]
		public void TestParser() 
		{
			string formatString = "yyyy MM dd HH:mm:ss.FFF";
			string valueString = "2010 05 01 02:03:04.567";
			int year = 2010;
			int month = 5;
			int day = 1;
			int hour = 2;
			int minute = 3;
			int second = 4;
			long nanoseconds = 567000000;

			LocalDateTime expected = LocalDateTime.Of (year, month, day, hour, minute, second, nanoseconds);
			LocalDateTime actual = LocalDateTime.Parse (valueString, DateTimeFormatter.OfPattern (formatString));
			Assert.IsTrue (expected.IsEqual(actual));
		}
	}
}

